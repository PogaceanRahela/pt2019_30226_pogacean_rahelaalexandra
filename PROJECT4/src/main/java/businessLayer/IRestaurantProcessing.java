package businessLayer;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public interface IRestaurantProcessing {

	// operatii admin

	public static void createBaseItem(String n, int id, double p) {

	}

	public static void createCompositeItem(String n, int id, double p, ArrayList<MenuItem> list) {

	}

	public static void deleteMenuItem(int idd) {

	}

	public static void editMenuItem(int idd, String newName, double newPrice) {

	}

	// operatii waiter
	public static void createOrder(Order order, int idd, int nrTable, ArrayList<MenuItem> list) {

	}

	public static double computePrice(int idd) {
		return 0;

	}

	public static String generateBill(int idCom) throws FileNotFoundException, UnsupportedEncodingException {
		return null;

	}

	void createMenuItem(MenuItem menuItem);

}
