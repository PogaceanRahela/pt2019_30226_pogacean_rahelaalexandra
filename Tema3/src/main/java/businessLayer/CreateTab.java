package businessLayer;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTable;

import model.Client;

public class CreateTab {
	
	public static JTable create(ArrayList<Client> list) {
        try {
        	
            Class<? extends Object> o = list.get(0).getClass();                
            Field[] cels = o.getDeclaredFields();            
            int nrCol = cels.length;
            String[] col = new String[nrCol];               
            Object[][] dat = new Object[list.size()][nrCol];
            
          /*  for(int i=0; i<cels.length; i++) {
            	col[i] = cels[i].getName();
            	
            }
            
            for(int j=0; j<obj.size(); j++) {
            	Field[] cels2 = obj.get(j).getClass().getDeclaredFields();
                
                for (int k = 0; k < nrCol; k++) {             
                    cels2[k].setAccessible(true);            
                    try {
                        dat[j][k] = cels2[k].get(obj.get(j));   
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
            }
            */
            int i=0;
            while(i<cels.length) {
            	
                col[i] = cels[i].getName();
                i++;
            }
            int j=0;
            
            while(j<list.size()) {         
                Field[] cels2 = list.get(j).getClass().getDeclaredFields();
               
                for (int k = 0; k < nrCol; k++) {             
                    cels2[k].setAccessible(true);            
                    try {
                        dat[j][k] = cels2[k].get(list.get(j));   
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                  
                }
                j++;
            }
            
            
            return new JTable(dat, col);    
            
        }
            catch (IndexOutOfBoundsException e) {
        	
            return new JTable(0, 0);      

        }

    }
	}

	
