package businessLayer;

import java.awt.Color;
import java.awt.Font;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import model.Client;

public class CreateTable {
	
	
	public static JTable makeTable(ArrayList<Client>obj) {
		if(obj==null) return null;
		
		ArrayList<String> headTableStrings = new ArrayList<String>(); //list: capul tablelului => trebuie sa luam numele campului din obiectul curent
		Object o=obj.get(0); //ne luam un obiect de tipul dat ca parametru
		for (Field field : o.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			headTableStrings.add(field.getName());
		}
	
		DefaultTableModel model = new DefaultTableModel();
		model.setColumnIdentifiers(obj.toArray()); //pune headerTable 
		
		JTable table = new JTable();
	
		table.setModel(model);
		//table = designTable(table);

		/*TableColumnModel tcm = table.getTableHeader().getColumnModel();
		for (int i = 0; i < obj.size(); i++) {
			TableColumn tc = tcm.getColumn(i);
			tc.setHeaderValue(obj.get(i));
		}*/
	
		String content[] = new String[headTableStrings.size()];
		
	//	System.out.println(headTableStrings);
		for(int i=0;i<obj.size();i++) {
			
				content[0]=obj.get(i).getId()+"";
				content[1]=obj.get(i).getName();
				content[2]=obj.get(i).getEmail();
				content[3]=obj.get(i).getAge()+"";
				
				model.addRow(content);
			}
		
		/**
		Iterator<String> s = headTableStrings.iterator();
		while (s.hasNext()) {
			for (i = 0; i < obj.size(); i++) {
				info[i] = s.next();
			}
			model.addRow(info);
		}
	*/
	return table;
	}
	

    }
	

	