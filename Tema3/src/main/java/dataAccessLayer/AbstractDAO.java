package dataAccessLayer;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.mysql.cj.xdevapi.Statement;

import model.Client;


public class AbstractDAO<T> {
	protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

	private final Class<T> type;

	@SuppressWarnings("unchecked")
	public AbstractDAO() {
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

	}
		protected String createSelectQuery(String field) {
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT ");
			sb.append(" * ");
			sb.append(" FROM ");
			sb.append(type.getSimpleName());
			sb.append(" WHERE " + field + " =?");
			return sb.toString();
		}
		
		/*protected String createSelectAll() {
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT ");
			sb.append(" * ");
			sb.append(" FROM ");
			sb.append(type.getSimpleName());
			return sb.toString();
		}
		*/
		
		private ArrayList<T> createObjects(ResultSet resultSet) {
			ArrayList<T> list = new ArrayList<T>();

			try {
				while (resultSet.next()) {
					T instance = type.newInstance();
					for (Field field : type.getDeclaredFields()) {
						Object value = resultSet.getObject(field.getName());
						PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
						Method method = propertyDescriptor.getWriteMethod();
						System.out.println(value);
						method.invoke(instance, value);
					}
					list.add(instance);
				}
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (IntrospectionException e) {
				e.printStackTrace();
			}
			return list;
		}
		
		public T findById(int id) {
			Connection connection = null;
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			String query = createSelectQuery("id");
			try {
				connection = ConnectionFactory.getConnection();
				statement = connection.prepareStatement(query);
				//statement.setInt(1, id);//
				resultSet = statement.executeQuery();

				return createObjects(resultSet).get(0);
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
			} finally {
				ConnectionFactory.close(resultSet);
				ConnectionFactory.close(statement);
				ConnectionFactory.close(connection);
			}
			return null;
		}

		
		public ArrayList<T> findAll() {
			
			Connection connection=null;
			PreparedStatement statement=null;
			ResultSet resultSet=null;
			
			String query = new String();
			query+=("SELECT ");
			query+=(" * ");
			query+=(" FROM ");
			query+=(type.getSimpleName());
			
			try {
				connection = ConnectionFactory.getConnection();
				statement = connection.prepareStatement(query);
				resultSet = statement.executeQuery();

				return createObjects(resultSet);
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, type.getName() + "DAO:findAll " + e.getMessage());
			} finally {
				ConnectionFactory.close(resultSet);
				ConnectionFactory.close(statement);
				ConnectionFactory.close(connection);
			}
			return null;
			
}		
		
		public void insert(T t) throws IllegalArgumentException, IllegalAccessException {
			
			Connection connection=null;
			PreparedStatement statement=null;
			
			String q= new String();
	        Field[] fs = t.getClass().getDeclaredFields();
	        q="INSERT INTO ";
	        q+=type.getSimpleName();                    
	        q+=" (";                                  
	        q+=fs[0].getName(); 
	        
	        for(int j=1; j<fs.length; j++) {
	        	q+=", " + fs[j].getName();
	        }
	        
	        q+=") ";                                    
	        q+="VALUES (?";  

	        for(int j=1; j<fs.length; j++) {
	        	q+=", ?";	
	        }
	        q+=")";
			
			try {
				connection = ConnectionFactory.getConnection();
				statement = connection.prepareStatement(q);
				
				 Field[] cels = t.getClass().getDeclaredFields(); 
		            int i=0;
		            while(i < cels.length) {
		                cels[i].setAccessible(true);                         
		                if (cels[i].getType().isAssignableFrom(String.class)) {       
		                	statement.setString(i + 1, cels[i].get(t).toString()); 
		                } else if(cels[i].getType().isAssignableFrom(Integer.class)){                                                      
		                	statement.setInt(i + 1, Integer.parseInt(cels[i].get(t).toString()));
		                }else {
		                	statement.setFloat(i + 1, Float.parseFloat(cels[i].get(t).toString()));
		                }
		                i++;
		                	
		            }
		            statement.executeUpdate();  
		            
			} catch (SQLException e) {
				//LOGGER.log(Level.WARNING, type.getName() + "DAO:findAll " + e.getMessage());
				System.out.println("Insert error!\n");
			} finally {
				ConnectionFactory.close(statement);
				ConnectionFactory.close(connection);
			}
			
}

	    public void update(T t, int id) throws IllegalArgumentException, IllegalAccessException {
	    	
	        Connection c = null;
	        PreparedStatement statement = null;
	        
	        String q= new String();
	        Field[] fields = t.getClass().getDeclaredFields();
	        q+="UPDATE ";
	        q+=type.getSimpleName();
	        q+=" SET ";
	        q+=fields[0].getName()+"=?";
	   
	        for(int j=1; j<fields.length; j++) {
	        	 q+=", " + fields[j].getName() + "= ?";
	        }
	        
	        q+=" WHERE " ;
	        q+= fields[0].getName() + "=" + fields;
	        q+= ";" ;
			
	        
	        try {
	            c = ConnectionFactory.getConnection();
	            statement = c.prepareStatement(q);            
	            Field[] cels = t.getClass().getDeclaredFields();  
	            int i=0;
	            while(i < cels.length) {
	                cels[i].setAccessible(true);         
	                
	                if (cels[i].getType().isAssignableFrom(String.class)) {       
	                	
	                	statement.setString(i + 1, cels[i].get(t).toString());  
	                }
	                else if(cels[i].getType().isAssignableFrom(Integer.class)){       
	                	statement.setInt(i + 1, Integer.parseInt(cels[i].get(t).toString()));
	                }
	                else {
	                	statement.setFloat(i + 1, Float.parseFloat(cels[i].get(t).toString()));
	                }
	                i++;
	            }
	            statement.executeUpdate(); 
	        } catch (SQLException e) {
	            System.out.println("Update error");
	        } finally {
	            ConnectionFactory.close(statement);
	            ConnectionFactory.close(c);
	        }
	    }
	    

	    public void delete(int id) {
	    	
	        Connection c = null;
	        PreparedStatement statement = null;
	       // String query = creareDelete(type.getDeclaredFields()[0].getName());
	        String q= new String();
	        //Field[] fields = t.getClass().getDeclaredFields();
	        String field = type.getDeclaredFields()[0].getName();
	        
	        q+="DELETE";
	        q+=" FROM ";
	        q+=type.getSimpleName();
	        q+=" WHERE ";
	        q+=field + " = ?";

	        try {
	            c = ConnectionFactory.getConnection();
	            statement = c.prepareStatement(q);
	            statement.setInt(1, id);     
	            statement.executeUpdate();
	        } catch (SQLException e) {
	            System.out.println("Delete error!!\n");
	        } finally {
	            ConnectionFactory.close(statement);
	            ConnectionFactory.close(c);
	        }
	    }


		/* public  <T>  JTable createTable (List<Object> lista)
		{
		
			JTable tabel = new JTable();
		
			//ArrayList<T>lista=new ArrayList<T>();
			
			ArrayList<String> coloane = new ArrayList<String>();
			Field[] fields = type.getDeclaredFields();
			for (Field f : fields )
			{	
				coloane.add(f.getName());
				
			}
			DefaultTableModel tableModel = new DefaultTableModel(coloane.toArray(),0);
			for(Object e : lista){
				Object[] data = new Object[coloane.size()];
				int k=0;
				for(Field f : e.getClass().getDeclaredFields()){
					f.setAccessible(true);
					Object o;
					try{
						o = f.get(e);
						data[k++]=o;
					}catch(Exception except){
						
					}
				}
				
				tableModel.addRow(data);
				tabel.setModel(tableModel);
			}
			
			return tabel;
		}
	*/
	}
