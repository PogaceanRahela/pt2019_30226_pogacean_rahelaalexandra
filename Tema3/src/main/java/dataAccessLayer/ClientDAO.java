package dataAccessLayer;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;

import model.Client;

public class ClientDAO extends AbstractDAO<Client> {

	public static void main(String[] args) throws SQLException, IllegalArgumentException, IllegalAccessException {
		
		
		ClientDAO client = new ClientDAO();
	
		//Client c = new Client(121, "Anaaa", "rahep@gmail.com", 23);
		Client c= new Client();
		c.setId(121);
		c.setName("Anaa");
		c.setEmail("rahep@gmail.ro");
		c.setAge(23);
		
		Client c2= new Client();
		c2.setId(133);
		c2.setName("Inna");
		c2.setEmail("marip@gmail.ro");
		c2.setAge(23);
		
		client.insert(c2);
		
		client.findAll();
		System.out.println(" \n\n");
		//client.update(c2, 000);
	
		
		//client.delete(121);
		client.findAll();
		
		//client.findById(121);
		
		//client.insert(c);
		
		//client.creareInsert(c);
		
		//client.createTable(null);//!!!!
		
	}



}
