package presentation;

import java.lang.reflect.*;
import java.awt.*;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import businessLayer.ClientBLL;
import businessLayer.CreateTab;
import businessLayer.CreateTable;
import dataAccessLayer.AbstractDAO;
import dataAccessLayer.ClientDAO;
import dataAccessLayer.ConnectionFactory;
import model.Client;

////////
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
//import java.util.List;

//////

	public class ClientWindow{//??? <T>
		
		public static JFrame frame;
		public  JButton add, delete, update, view, back;
		public JTextField textField_1, textField_2, textField_3, textField_4;
		private JScrollPane scrollPane;
		private MainWindow mw;
		private JTable tabel;
		
		//private static ClientDAO cc;
		
		//private static List<Object> lista;//

		//DefaultTableModel table = new DefaultTableModel();
		
		//private final Class<T> type;
		
		public ClientWindow() throws SQLException {
			
			frame=new JFrame();
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setSize(new Dimension(1500, 900));
			
			JLabel id = new JLabel("ID: ");
			id.setBounds(45, 50, 500, 40);
			id.setFont(new Font("Times New Roman", Font.ITALIC, 25));
			
			JLabel n = new JLabel("NAME: ");
			n.setBounds(45, 120, 500, 40);
			n.setFont(new Font("Times New Roman", Font.ITALIC, 25));
			
			JLabel e = new JLabel("EMAIL: ");
			e.setBounds(45, 190, 500, 40);
			e.setFont(new Font("Times New Roman", Font.ITALIC, 25));
			
			JLabel a = new JLabel("AGE: ");
			a.setBounds(45, 260, 500, 40);
			a.setFont(new Font("Times New Roman", Font.ITALIC, 25));
			
			textField_1 = new JTextField(20);
			textField_1.setBounds(140, 55, 270, 30);
			textField_1.setColumns(10);
			
			textField_2 = new JTextField(20);
			textField_2.setBounds(140, 125, 270, 30);
			textField_2.setColumns(10);
			
			textField_3 = new JTextField(20);
			textField_3.setBounds(140, 195, 270, 30);
			textField_3.setColumns(10);
			
			textField_4 = new JTextField(20);
			textField_4.setBounds(140, 265, 270, 30);
			textField_4.setColumns(10);

			
			add = new JButton("ADD");
			add.setBackground(Color.LIGHT_GRAY);
			add.setForeground(Color.RED);
			add.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 14));
			add.setBounds(150, 350, 200, 40);
			
			update = new JButton("UPDATE");
			update.setBackground(Color.LIGHT_GRAY);
			update.setForeground(Color.RED);
			update.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 14));
			update.setBounds(150, 450, 200, 40);
			
			delete = new JButton("DELETE");
			delete.setBackground(Color.LIGHT_GRAY);
			delete.setForeground(Color.RED);
			delete.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 14));
			delete.setBounds(150, 550, 200, 40);
			
			view = new JButton("VIEW");
			view.setBackground(Color.LIGHT_GRAY);
			view.setForeground(Color.RED);
			view.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 14));
			view.setBounds(150, 650, 200, 40);
			
			back = new JButton("BACK");
			back.setBackground(Color.LIGHT_GRAY);
			back.setForeground(Color.RED);
			back.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 14));
			back.setBounds(150, 750, 200, 40);
			
				//ClientBLL clientBll = new ClientBLL();
			
				final ClientDAO cc=new ClientDAO();
				 
				//final ArrayList<Client> clienti = new ArrayList<Client>();
				//c.findAll();
			
			
			///////////////
			/*ClientDAO cc = new ClientDAO();
			cc.findAll();			
			
			ArrayList<Client> clienti = new ArrayList<Client>();
			String statement = "SELECT * FROM client";
			Client toReturn = null;
			
			ArrayList <String> coloane = new ArrayList <String> (10);
			String[] col = new String [coloane.size()];
			

			Connection dbConnection = ConnectionFactory.getConnection();//???
			PreparedStatement findStatement = null;
			ResultSet rs = null;

			DefaultTableModel tableModel = new DefaultTableModel();
			Object client = new Client ();
			Field[] fields = client.getClass().getDeclaredFields();
			for (Field f : fields )
			{	
				coloane.add(f.getName());
			
			}
			for (int i=0;i<coloane.size();i++)
			{
				System.out.println(coloane.get(i));
			}
			
			try {
				findStatement = dbConnection.prepareStatement(statement);
				rs = findStatement.executeQuery();
				while(rs.next())
				{
					int idc = Integer.parseInt(rs.getString("id"));
					String namec = rs.getString("name");
					String emailc = rs.getString("email");
					int agec = Integer.parseInt(rs.getString("age"));
					
					toReturn = new Client(idc, namec, email, agec);
					clienti.add(toReturn);
	
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			};
			
			col=coloane.toArray(col);
			
			for(String s : col)
			    System.out.println(s);
			

			MyTableModel m = new MyTableModel ();
			tableModel=m.creare(client.getClass(),clienti);
			
			if(scrollPane != null)
				panou.remove(scrollPane);
			scrollPane = new JScrollPane();
			scrollPane.setBounds(356, 109, 329, 154);
			panou.add(scrollPane);
			
			table = new JTable(tableModel);
			//panouT.setVisible(true);
			//scrollPane.add(table);
			scrollPane.setViewportView(table);
	
}

				//clienti = clientBll.findAll();
				
				/*DefaultTableModel model = new DefaultTableModel();
				model=c.createTable();
				
				JTable tabel = new JTable();
				tabel.setModel(model);
				
				DefaultTableModel  m = new DefaultTableModel  ();
				
				String[] coloane = {"ID","Name","Email","Age"};	
				
				model.addColumn("id");
				model.addColumn("name");
				model.addColumn("email");
				model.addColumn("age");
				model.addRow(coloane);	
				
				for(Client client: clienti){
					Object[] row = new Object[4];
					row[0] = client.getId();
					row[1] = client.getName();
					row[2] = client.getEmail(); 
					row[3] = client.getAge(); 
					model.addRow(row);
				}
				tabel.setModel(model);
				*/
				//ArrayList<Client>aaa=new ArrayList<Client>();
				
				ArrayList<Client>aaa=new ArrayList<Client>();
				tabel = new JTable();
				aaa.add(new Client(222,"Ana","ss",22));
				CreateTable.makeTable(aaa);
				//CreateTab.create(aaa);
				//scrollPane= new JScrollPane();
				//scrollPane.add(tabel);
				tabel.setVisible(true);
				//scrollPane.setViewportView(tabel);
				tabel.setBounds(500, 80, 900, 680);
				//scrollPane.setBounds(500, 80, 900, 680);
				//scrollPane.setVisible(true);
				
		
			final JPanel panel = new JPanel();
			panel.setLayout(null);
			panel.setBounds(26,105, 300, 315);
			
			panel.add(id);
			panel.add(n);
			panel.add(e);
			panel.add(a);
			panel.add(textField_1);
			panel.add(textField_2);
			panel.add(textField_3);
			panel.add(textField_4);
			panel.add(add);
			panel.add(update);
			panel.add(delete);
			panel.add(view);
			panel.add(back);
			
			//panel.add(scrollPane);
			panel.add(tabel);
			
			 frame.add(panel);	
			 frame.setVisible(true);

			//panel.add(tabel);
		
			
			//ClientBLL clientBll = new ClientBLL();
			
			 //final ClientDAO c=new ClientDAO();
			 
			//ArrayList<Client> clienti = new ArrayList<Client>();
			 view.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent et) {
			
						//mw.main(null);
						//ClientDAO cc = new ClientDAO();
						ArrayList<Client> list=cc.findAll();
						ArrayList<Object>obj=new ArrayList<Object>();
						
						panel.remove(tabel);
						//scrollPane.remove(tabel);
						tabel=CreateTable.makeTable(list);
						//tabel=CreateTab.create(list);
						
						//scrollPane.add(tabel);
						panel.add(tabel);
						frame.setVisible(true);
				
					}
				});
		
			
			 back.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent et) {
			
						try {
							mw.main(null);
						} catch (SQLException e) {
							e.printStackTrace();
						}
						frame.setVisible(false);
				
					}
				});
	
			 
		}
		
		public static void main(String[] args) throws SQLException {
			
			new ClientWindow();
			
		}

/*
	public void adaugareTabel() throws SQLException  {
		//ClientBLL clientBll = new ClientBLL();
		final ClientDAO cc =new ClientDAO();
		
		ArrayList<Client> clienti = new ArrayList<Client>();
		//clienti = clientBll.getClienti();

		String[] coloane = {"ID","Name","Email","Age"};		
		DefaultTableModel model = new DefaultTableModel();
		
		model.addColumn("id");
		model.addColumn("name");
		model.addColumn("email");
		model.addColumn("age");
		model.addRow(coloane);	
		
		for(Client client: clienti){
			Object[] row = new Object[4];
			row[0] = client.getId();
			row[1] = client.getName();
			row[2] = client.getEmail(); 
			row[3] = client.getAge(); 
			model.addRow(row);
		}
		
		tabel.setModel(model);
	}
	*/
		
	}
	



