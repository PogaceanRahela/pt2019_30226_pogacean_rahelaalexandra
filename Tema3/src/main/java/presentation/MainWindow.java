package presentation;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import businessLayer.ClientBLL;
import businessLayer.OrderBLL;
import businessLayer.ProductBLL;
import dataAccessLayer.ClientDAO;
import dataAccessLayer.ConnectionFactory;
import dataAccessLayer.OrderDAO;
import dataAccessLayer.ProductDAO;
import model.Client;

public class MainWindow{
	
	public static JFrame frame, secondFr;
	public static JButton cc, pp;
	//private JTable tabel;
	
	public static void main(String[] args) throws SQLException {
		MainWindow mw=new MainWindow();
		
		/*ClientDAO cd =new ClientDAO();
		ProductDAO pd=new ProductDAO();
		OrderDAO od=new OrderDAO();
		
		ClientBLL cb=new ClientBLL();
		ProductBLL pb=new ProductBLL();
		OrderBLL ob=new OrderBLL();
		
		new Main(mw, cb, pb, ob, cd, pd, od);*/
	}
	public MainWindow() throws SQLException {
	
		frame=new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(new Dimension(1500, 900));
		
		 //tabel= new JTable();
		
		JLabel o = new JLabel("Choose an option! ");
		o.setBounds(43, 40, 500, 40);
		o.setFont(new Font("Times New Roman", Font.ITALIC, 25));
		
		cc = new JButton("Customer Management");
		cc.setBackground(Color.LIGHT_GRAY);
		cc.setForeground(Color.RED);
		cc.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 14));
		cc.setBounds(42, 130, 200, 40);
		
		pp = new JButton("Product Management");
		pp.setBackground(Color.LIGHT_GRAY);
		pp.setForeground(Color.RED);
		pp.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 14));
		pp.setBounds(42, 200, 200, 40);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.add(o);
		panel.add(cc);
		panel.add(pp);
		panel.setBounds(26,105, 300, 315);
		 ImageIcon icon=new ImageIcon("order.jpg");
		 JLabel img=new JLabel(icon);
		 img.add(panel);
		 frame.setContentPane(img);	
		 frame.setVisible(true);
		 
		 cc.addActionListener(new ActionListener(){

				public void actionPerformed(ActionEvent e) {
					try {
						new ClientWindow();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
					
				}
			});
		 
		 pp.addActionListener(new ActionListener(){

				public void actionPerformed(ActionEvent e) {
					new ProductWindow();
					
				}
			});
	
		  
	}
	public void setjTable(JTable create) {
		// TODO Auto-generated method stub
		
	}

	}

