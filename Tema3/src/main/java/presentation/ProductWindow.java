package presentation;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.*;
import model.Client;

	public class ProductWindow{
		
		public  JFrame frame;
		public  JButton add, delete, update, view, back;
		public JTextField textField_1, textField_2, textField_3, textField_4;
		private MainWindow mw;
		
		public ProductWindow() {
			
			frame=new JFrame();
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setSize(new Dimension(1500, 900));
			
			JLabel id = new JLabel("ID: ");
			id.setBounds(45, 50, 500, 40);
			id.setFont(new Font("Times New Roman", Font.ITALIC, 25));
			
			JLabel n = new JLabel("NAME: ");
			n.setBounds(45, 120, 500, 40);
			n.setFont(new Font("Times New Roman", Font.ITALIC, 25));
			
			JLabel q = new JLabel("QUANTITY:");
			q.setBounds(45, 190, 500, 40);
			q.setFont(new Font("Times New Roman", Font.ITALIC, 25));
			
			JLabel p = new JLabel("PRICE: ");
			p.setBounds(45, 260, 500, 40);
			p.setFont(new Font("Times New Roman", Font.ITALIC, 25));
			
			textField_1 = new JTextField(20);
			textField_1.setBounds(185, 55, 270, 30);
			textField_1.setColumns(10);
			
			textField_2 = new JTextField(20);
			textField_2.setBounds(185, 125, 270, 30);
			textField_2.setColumns(10);
			
			textField_3 = new JTextField(20);
			textField_3.setBounds(185, 195, 270, 30);
			textField_3.setColumns(10);
			
			textField_4 = new JTextField(20);
			textField_4.setBounds(185, 265, 270, 30);
			textField_4.setColumns(10);

			
			add = new JButton("ADD");
			add.setBackground(Color.LIGHT_GRAY);
			add.setForeground(Color.RED);
			add.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 14));
			add.setBounds(180, 350, 200, 40);
			
			update = new JButton("UPDATE");
			update.setBackground(Color.LIGHT_GRAY);
			update.setForeground(Color.RED);
			update.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 14));
			update.setBounds(180, 450, 200, 40);
			
			delete = new JButton("DELETE");
			delete.setBackground(Color.LIGHT_GRAY);
			delete.setForeground(Color.RED);
			delete.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 14));
			delete.setBounds(180, 550, 200, 40);
			
			view = new JButton("VIEW");
			view.setBackground(Color.LIGHT_GRAY);
			view.setForeground(Color.RED);
			view.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 14));
			view.setBounds(180, 650, 200, 40);
			
			back = new JButton("BACK");
			back.setBackground(Color.LIGHT_GRAY);
			back.setForeground(Color.RED);
			back.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 14));
			back.setBounds(180, 750, 200, 40);
			
			JPanel panel = new JPanel();
			panel.setLayout(null);
			panel.setBounds(26,105, 300, 315);
			
			panel.add(id);
			panel.add(n);
			panel.add(q);
			panel.add(p);
			panel.add(textField_1);
			panel.add(textField_2);
			panel.add(textField_3);
			panel.add(textField_4);
			panel.add(add);
			panel.add(update);
			panel.add(delete);
			panel.add(view);
			panel.add(back);
		
			frame.setContentPane(panel);	
			frame.setVisible(true);
			
			 back.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent et) {
			
						try {
							mw.main(null);
						} catch (SQLException e) {
							e.printStackTrace();
						}
						frame.setVisible(false);
						
			
					}
				});

		}
		
		public static void main(String[] args) {
			new ProductWindow();
		}

}



